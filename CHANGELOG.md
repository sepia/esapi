# [1.1.0](https://gitlab.gwdg.de/sepia/esapi/compare/v1.0.0...v1.1.0) (2021-06-17)


### Bug Fixes

* **utils:request-builder:** call `utils:body-builder($body)` instead of printing it ([2b9a898](https://gitlab.gwdg.de/sepia/esapi/commit/2b9a898b7f6ae706ce6f050fb65f1ae5c1e5a82c))


### Features

* **document:** provide document api functions ([6e53d07](https://gitlab.gwdg.de/sepia/esapi/commit/6e53d073df4c58a23b3b0335dc562afba6e40ca8))
* **index:** provide index api functions ([901ba98](https://gitlab.gwdg.de/sepia/esapi/commit/901ba98d99935529f2d2b754516ddd88f848e0de))
* **search:** provide search api functions ([71cda68](https://gitlab.gwdg.de/sepia/esapi/commit/71cda6864f00a6ef4dea87ece1104f413b85e725))
* **utils.xqm:** provide module with utilities to send requests ([696ed02](https://gitlab.gwdg.de/sepia/esapi/commit/696ed02d5ae56a9227693496bdfc318dab44dd24))

# 1.0.0 (2021-06-03)


### Features

* **legacy-api:** provide legacy api for Architrave and KMG projects ([12c127d](https://gitlab.gwdg.de/sepia/esapi/commit/12c127d44ec3313b2620db0411c7dfb3d4921634)), closes [#1](https://gitlab.gwdg.de/sepia/esapi/issues/1)
