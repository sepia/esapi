(:~
 : ElasticSearch Search API Module
 :
 : Provides functions to search indices.
 :
 : @author Stefan Hynek
 :)
xquery version "3.1";

module namespace search="http://sepia.io/esapi/search";

import module namespace utils="http://sepia.io/esapi/utils";

(: api endpoint for all basic operations :)
declare variable $search:default-api := "_search";
(: querying all indices does not require an index id :)
declare variable $search:default-index := "";
(: querying an index does not require a document id :)
declare variable $search:default-id := "";
(: default query parameters :)
declare variable $search:default-qpar := map {};

(:~
 : Query an index.
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/search-search.html
 :
 : @param $service ElasticSearch service address
 : @param $index index
 : @param $qpar a map of query parameters, might be the empty map (map {})
 : @param $body the request body as a map
 : @return response body parsed into a map
 :)
declare function search:query
    (
    $service as xs:string,
    $index as xs:string,
    $qpar as map(*),
    $body as map(*)
    ) 
as map(*)
{
    fn:parse-json(utils:post(xs:anyURI($service), $index, $search:default-api, $search:default-id, $qpar, $body)[2])
};

(:~
 : Query all indices.
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/search-search.html
 :
 : @param $service ElasticSearch service address
 : @param $qpar a map of query parameters, might be the empty map (map {})
 : @param $body the request body as a map
 : @return response body parsed into a map
 :)
declare function search:query-all
    (
        $service as xs:string,
        $qpar as map(*),
        $body as map(*)
    )
as map(*)
{
    fn:parse-json(search:query($service, $search:default-index, $qpar, $body)[2])
};


(: ----------------------------------------------------------------------------
 : Functions with defaults set to provide simpler function signatures.
 :)


(:~
 : Query an index.
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/search-search.html
 :
 : @param $service ElasticSearch service address
 : @param $index index
 : @param $body the request body as a map
 : @return response body parsed into a map
 :)
declare function search:query
    (
    $service as xs:string,
    $index as xs:string,
    $body as map(*)
    )
as map(*)
{
    search:query($service, $index, $search:default-qpar, $body)
};

(:~
 : Query all indices.
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/search-search.html
 :
 : @param $service ElasticSearch service address
 : @param $body the request body as a map
 : @return response body parsed into a map
 :)
declare function search:query-all
    (
    $service as xs:string,
    $body as map(*)
    )
as map(*)
{
    search:query-all($service, $search:default-qpar, $body)
};
