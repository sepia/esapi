(:~
 : ElasticSearch API Utilities Module
 :
 : Provides wrapper functions around http:send-request for all applicable HTTP
 : verbs with the appropriate parameters as well as helper functions for
 : building query strings, message bodies and request elements.
 :
 : @author Stefan Hynek
 :)
module namespace utils="http://sepia.io/esapi/utils";

declare namespace http="http://expath.org/ns/http-client"; (: as of exist-db 5.2.0 => v1.2.2 :)
declare namespace map="http://www.w3.org/2005/xpath-functions/map";

declare variable $utils:default-serialization-options := map { "method" : "json" };


(:~
 : Take a map of optional Query-Parameters to the ES-API and serialize them
 : into a Query-String.
 :
 : @author Stefan Hynek
 : @param $qpar a map of query options, might be the empty map (map {})
 : @return the query string or, if the map was empty, the empty string
 :)
declare function utils:query-builder
    (
        $qpar as map(*)
    )
as xs:string
{
    if (map:size($qpar) > 0) then
    concat(
        "?",
        string-join(
            map:for-each(
                $qpar,
                function($key as xs:string, $value  as xs:string) (: this is a lambda function; cool, eh? :)
                {
                    concat($key, "=", $value)
                }
            ),
            "&amp;"
        )
    )
    else ""
};

(:~
 : Take a map of Options to the ES-API and serialize them into a http
 : body element with JSON content.
 :
 : @author Stefan Hynek
 : @param $body a map of options, might be the empty map (map {})
 : @return http body element or empty sequence
 :)
declare function utils:body-builder
    (
        $body as map(*)
    )
as element(http:body)?
{
    if (map:size($body) > 0) then
        <http:body media-type="application/json" method="text">
        {
            serialize($body, $utils:default-serialization-options)
        }
        </http:body>
    else
        ()
};

(:~
 : Build a http request object from all kinds of different parameters.
 :
 : @author Stefan Hynek
 : @param $service
 : @param $request-method the HTTP request method
 : @param $target index or datastream
 : @param $api-endpoint api endpoint
 : @param $id document id
 : @param $qpar a map of query parameters, might be the empty map (map {})
 : @param $body a map of options, might be the empty map (map {})
 : @return http request element
 :)
declare function utils:request-builder
    (
        $service as xs:anyURI,
        $request-method as xs:string,
        $target as xs:string,
        $api-endpoint as xs:string,
        $id as xs:string,
        $qpar as map(*),
        $body as map(*)
    )
as element(http:request)
{
    <http:request
        method="{$request-method}"
        href="{$service}/{$target}/{$api-endpoint}/{$id}{utils:query-builder($qpar)}"
        (: the current implementation of http client will always handle responses
         : of the "application/json" media type as binary content => override it
         : with the non-existent media type "text/json"
         :)
        override-media-type="text/json"
    >
    {
        utils:body-builder($body)
    }
    </http:request>
};

(:~
 : A wrapper around `http:send-request` to allow for consistent error handling.
 :)
declare function utils:send-request
    (
        $service as xs:anyURI,
        $request-method as xs:string,
        $target as xs:string,
        $api-endpoint as xs:string,
        $id as xs:string,
        $qpar as map(*),
        $body as map(*)
    )
as item()*
{
    try {
        http:send-request(
            utils:request-builder($service, $request-method, $target, $api-endpoint, $id, $qpar, $body)
        )
    } catch * {
        (: The current exist-db implementation of the http client does not follow
        : the specification in terms of the defined errors. Must catch and handle
        : *all* errors by itself.
        :)
        fn:error(xs:QName("utils:ErrorName"), "Non-defined error occured.")
    }
};


(: ----------------------------------------------------------------------------
 : The following generic functions send a request for each of the used HTTP
 : verbs. The function signatures are identical except for `post` and `put`,
 : which allow the indication of a message body.
 :)


(:~
 : Generic DELETE function.
 :
 : @author Stefan Hynek
 :
 : @param $service as xs:anyURI
 : @param $target as xs:string
 : @param $api-endpoint as xs:string
 : @param $id as xs:string
 : @param $qpar as map(*)
 :)
declare function utils:delete
    (
        $service as xs:anyURI,
        $target as xs:string,
        $api-endpoint as xs:string,
        $id as xs:string,
        $qpar as map(*)
    )
as item()*
{
    utils:send-request($service, "DELETE", $target, $api-endpoint, $id, $qpar, map {})
};

(:~
 : Generic GET function.
 :
 : @author Stefan Hynek
 :
 : @param $service as xs:anyURI
 : @param $target as xs:string
 : @param $api-endpoint as xs:string
 : @param $id as xs:string
 : @param $qpar as map(*)
 :)
declare function utils:get
    (
        $service as xs:anyURI,
        $target as xs:string,
        $api-endpoint as xs:string,
        $id as xs:string,
        $qpar as map(*)
    )
as item()*
{
    utils:send-request($service, "GET", $target, $api-endpoint, $id, $qpar, map {})
};

(:~
 : Generic HEAD function.
 :
 : @author Stefan Hynek
 :
 : @param $service as xs:anyURI
 : @param $target as xs:string
 : @param $api-endpoint as xs:string
 : @param $id as xs:string
 : @param $qpar as map(*)
 :)
declare function utils:head
    (
        $service as xs:anyURI,
        $target as xs:string,
        $api-endpoint as xs:string,
        $id as xs:string,
        $qpar as map(*)
    )
as element(http:response)
{
    utils:send-request($service, "HEAD", $target, $api-endpoint, $id, $qpar, map {})
};

(:~
 : Generic POST function.
 :
 : @author Stefan Hynek
 :
 : @param $service as xs:anyURI
 : @param $target as xs:string
 : @param $api-endpoint as xs:string
 : @param $id as xs:string
 : @param $qpar as map(*)
 : @param $body as map(*)
 :)
declare function utils:post
    (
        $service as xs:anyURI,
        $target as xs:string,
        $api-endpoint as xs:string,
        $id as xs:string,
        $qpar as map(*),
        $body as map(*)
    )
as item()*
{
    utils:send-request($service, "POST", $target, $api-endpoint, $id, $qpar, $body)
};

(:~
 : Generic PUT function.
 :
 : @author Stefan Hynek
 :
 : @param $service as xs:anyURI
 : @param $target as xs:string
 : @param $api-endpoint as xs:string
 : @param $id as xs:string
 : @param $qpar as map(*)
 : @param $body as map(*)
 :)
declare function utils:put
    (
        $service as xs:anyURI,
        $target as xs:string,
        $api-endpoint as xs:string,
        $id as xs:string,
        $qpar as map(*),
        $body as map(*)
    )
as item()*
{
    utils:send-request($service, "PUT", $target, $api-endpoint, $id, $qpar, $body)
};
