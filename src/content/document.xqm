(:~
 : ElasticSearch Document API Module
 :
 : Provides functions to CRUD documents.
 :
 : @author Stefan Hynek
 :)
xquery version "3.1";

module namespace document="http://sepia.io/esapi/document";

import module namespace utils="http://sepia.io/esapi/utils";

(: api endpoint for all basic operations :)
declare variable $document:default-api := "_doc";
(: api endpoint for update operations :)
declare variable $document:update-api := "_update";
(: default query parameters :)
declare variable $document:default-qpar := map {};


(:~
 : Creates or replaces a document depending on whether the id exists
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-index_.html
 :
 : @param $service ElasticSearch service address
 : @param $target index or data stream
 : @param $id the document id
 : @param $qpar a map of query parameters, might be the empty map (map {})
 : @param $body the request body as a map
 : @return response body parsed into a map
 :)
declare function document:add
    (
        $service as xs:string,
        $target as xs:string,
        $id as xs:string,
        $qpar as map(*),
        $body as map(*)
    )
as map(*)
{
    fn:parse-json(
        utils:post($service, $target, $document:default-api, $id, $qpar, $body)[2]
    )
};

(:~
 : Retrieve a document.
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-get.html
 :
 : @param $service ElasticSearch service address
 : @param $index index
 : @param $id the document id
 : @param $qpar a map of query parameters, might be the empty map (map {})
 : @return response body parsed into a map
 :)
declare function document:retrieve
    (
        $service as xs:string,
        $index as xs:string,
        $id as xs:string,
        $qpar as map(*)
    )
as map(*)
{
    fn:parse-json(
        utils:get(xs:anyURI($service), $index, $document:default-api, $id, $qpar)[2]
    )
};

(:~
 : Check the existence of a document.
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-get.html
 :
 : @param $service ElasticSearch service address
 : @param $index
 : @param $id document id
 : @param $qpar a map of query parameters, might be the empty map (map {})
 : @return true if document exists, false otherwise
 :)
declare function document:exists
    (
        $service as xs:string,
        $index as xs:string,
        $id as xs:string,
        $qpar as map(*)
    )
as xs:boolean
{
    utils:head(xs:anyURI($service), $index, $document:default-api, $id, $qpar)/@status = 200
};

(:~
 : Remove a document.
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-delete.html
 :
 : @param $service ElasticSearch service address
 : @param $index index
 : @param $id the document id
 : @param $qpar a map of query parameters, might be the empty map (map {})
 : @return response body parsed into a map
 :)
declare function document:remove
    (
        $service as xs:string,
        $index as xs:string,
        $id as xs:string,
        $qpar as map(*)
    )
as map(*)
{
    fn:parse-json(
        utils:delete(xs:anyURI($service), $index, $document:default-api, $id, $qpar)[2]
    )
};

(:~
 : Update a document.
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-update.html
 :
 : @param $service ElasticSearch service address
 : @param $index index
 : @param $id the document id
 : @param $qpar a map of query parameters, might be the empty map (map {})
 : @param $body the request body as a map
 : @return response body parsed into a map
 :)
declare function document:update
    (
        $service as xs:string,
        $index as xs:string,
        $id as xs:string,
        $qpar as map(*),
        $body as map(*)
    )
as map(*)
{
    fn:parse-json(
        utils:post(xs:anyURI($service), $index, $document:default-api, $id, $qpar, $body)[2]
    )
};


(: ----------------------------------------------------------------------------
 : Functions with defaults set to provide simpler function signatures.
 :)


(:~
 : Creates or replaces a document depending on whether the id exists
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-index_.html
 :
 : @param $service ElasticSearch service address
 : @param $target index or data stream
 : @param $id the document id
 : @param $body the request body as a map
 : @return response body parsed into a map
 :)
declare function document:add
    (
        $service as xs:string,
        $target as xs:string,
        $id as xs:string,
        $body as map(*)
    )
as map(*)
{
    document:add($service, $target, $id, $document:default-qpar, $body)
};

(:~
 : Retrieve a document.
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-get.html
 :
 : @param $service ElasticSearch service address
 : @param $index index
 : @param $id the document id
 : @return response body parsed into a map
 :)
declare function document:retrieve
    (
        $service as xs:string,
        $index as xs:string,
        $id as xs:string
    )
as map(*)
{
    document:retrieve($service, $index, $id, $document:default-qpar)
};

(:~
 : Check the existence of a document.
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-get.html
 :
 : @param $service ElasticSearch service address
 : @param $index
 : @param $id document id
 : @return true if document exists, false otherwise
 :)
declare function document:exists
    (
        $service as xs:string,
        $index as xs:string,
        $id as xs:string
    )
as xs:boolean
{
    document:exists($service, $index, $id, $document:default-qpar)
};

(:~
 : Remove a document.
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-delete.html
 :
 :  @param $service ElasticSearch service address
 : @param $index index
 :  @param $id the document id
 :  @return response body parsed into a map
 :)
declare function document:remove
    (
        $service as xs:string,
        $index as xs:string,
        $id as xs:string
    )
as map(*)
{
    document:remove($service, $index, $id, $document:default-qpar)
};

(:~
 : Update a document.
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-update.html
 :
 : @param $service ElasticSearch service address
 : @param $index index
 : @param $id the document id
 : @param $body the request body as a map
 : @return response body parsed into a map
 :)
declare function document:update
    (
        $service as xs:string,
        $target as xs:string,
        $id as xs:string,
        $body as map(*)
    )
as map(*)
{
    document:update($service, $target, $id, $document:default-qpar, $body)
};