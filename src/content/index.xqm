(:~
 : ElasticSearch Index API Module
 :
 : Provides functions to CRUD indices. Functions with a `$target` parameter can
 : also be used on data streams.
 :
 : @author Stefan Hynek
 :)
xquery version "3.1";

module namespace index="http://sepia.io/esapi/index";

import module namespace utils="http://sepia.io/esapi/utils";

(: api endpoint for all basic operations :)
declare variable $index:default-api := "";
(: api endpoint for refresh operations :)
declare variable $index:refresh-api := "_refresh";
(: operating on an all indices does not require an index id :)
declare variable $index:default-index := "";
(: operating on an index does not require a document id :)
declare variable $index:default-document := "";
(: default query parameters :)
declare variable $index:default-qpar := map {};
(: default request body options :)
declare variable $index:default-body := map {};


(:~
 : Create an index.
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-create-index.html
 :
 : @param $service ElasticSearch service address
 : @param $index index to create
 : @param $qpar a map of query parameters, might be the empty map (map {})
 : @param $body the request body as a map
 : @return response body parsed into a map
 :)
declare function index:create
    (
        $service as xs:string,
        $index as xs:string,
        $qpar as map(*),
        $body as map(*)
    )
as map(*)
{
    fn:parse-json(utils:put(xs:anyURI($service), $index, $index:default-api, $index:default-document, $qpar, $body)[2])
};

(:~
 : Remove an index.
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-delete-index.html
 :
 : @param $service ElasticSearch service address
 : @param $index index to remove
 : @param $qpar a map of query parameters, might be the empty map (map {})
 : @return response body parsed into a map
 :)
declare function index:remove
    (
        $service as xs:string,
        $index as xs:string,
        $qpar as map(*)
    )
as map(*)
{
      fn:parse-json(utils:delete(xs:anyURI($service), $index, $index:default-api, $index:default-document, $qpar)[2])
};

(:~
 : Get information about an index or a data stream.
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-get-index.html
 :
 : @param $service ElasticSearch service address
 : @param $target index or data stream to get information about
 : @param $qpar a map of query parameters, might be the empty map (map {})
 : @return response body parsed into a map
 :)
declare function index:get
    (
        $service as xs:string,
        $target as xs:string,
        $qpar as map(*)
    )
as map(*)
{
    fn:parse-json(utils:get(xs:anyURI($service), $target, $index:default-api, $index:default-document, $qpar)[2])
};

(:~
 : Check the existence of an index or a data stream.
 :
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-exists.html
 :
 : @param $service ElasticSearch service address
 : @param $target index or data stream to check
 : @param $qpar a map of query parameters, might be the empty map (map {})
 : @return true if (all) target(s) exist(s), false otherwise
 :)
declare function index:exists
    (
        $service as xs:string,
        $target as xs:string,
        $qpar as map(*)
    )
as xs:boolean
{
    utils:head(xs:anyURI($service), $target, $index:default-api, $index:default-document, $qpar)/@status = 200
};

(:~
 : Refresh an index or data stream.
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/7.13/indices-refresh.html
 :
 : @param $service ElasticSearch service address
 : @param $target index or data stream to refresh
 : @param $qpar a map of query parameters, might be the empty map (map {})
 : @return response body parsed into a map
 :)
declare function index:refresh
    (
        $service as xs:string,
        $target as xs:string,
        $qpar as map(*)
    )
as map(*)
{
    fn:parse-json(utils:get(xs:anyURI($service), $target, $index:refresh-api, $index:default-document, $qpar)[2])
};


(: ----------------------------------------------------------------------------
 : Functions with defaults set to provide simpler function signatures.
 :)


(:~
 : Create an index.
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-create-index.html
 :
 : @param $service ElasticSearch service address
 : @param $index index to create
 : @return response body parsed into a map
 :)
declare function index:create
    (
        $service as xs:string,
        $index as xs:string
    )
as map(*)
{
    index:create($service, $index, $index:default-qpar, $index:default-body)
};

(:~
 : Create an index.
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-create-index.html
 :
 : @param $service ElasticSearch service address
 : @param $index index to create
 : @param $body the request body as a map
 : @return response body parsed into a map
 :)
declare function index:create
    (
        $service as xs:string,
        $index as xs:string,
        $body as map(*)
    )
as map(*)
{
    index:create($service, $index, $index:default-qpar, $body)
};

(:~
 : Remove an index.
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-delete-index.html
 :
 : @param $service ElasticSearch service address
 : @param $index index to remove
 : @return response body parsed into a map
 :)
declare function index:remove
    (
        $service as xs:string,
        $index as xs:string
    )
as map(*)
{
    index:remove($service, $index, $index:default-qpar)
};

(:~
 : Get information about an index or a data stream.
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-get-index.html
 :
 : @param $service ElasticSearch service address
 : @param $target index or data stream to get information about
 : @return response body parsed into a map
 :)
declare function index:get
    (
        $service as xs:string,
        $target as xs:string
    )
as map(*)
{
    index:get($service, $target, $index:default-qpar)
};

(:~
 : Check the existence of an index or a data stream.
 :
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-exists.html
 :
 : @param $service ElasticSearch service address
 : @param $target index or data stream to check
 : @return true if (all) target(s) exist(s), false otherwise
 :)
declare function index:exists
    (
        $service as xs:string,
        $target as xs:string
    )
as xs:boolean
{
    index:exists($service, $target, $index:default-qpar)
};

(:~
 : Refresh an index or data stream.
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/7.13/indices-refresh.html
 :
 : @param $service ElasticSearch service address
 : @param $target index or data stream to refresh
 : @return response body parsed into a map
 :)
declare function index:refresh
    (
        $service as xs:string,
        $target as xs:string
    )
as map(*)
{
    index:refresh($service, $target, $index:default-qpar)
};

(:~
 : Refresh all indices and data streams.
 :
 : @author Stefan Hynek
 : @see https://www.elastic.co/guide/en/elasticsearch/reference/7.13/indices-refresh.html
 :
 : @param $service ElasticSearch service address
 : @return response body parsed into a map
 :)
declare function index:refresh
    (
        $service as xs:string
    )
as map(*)
{
    index:refresh($service, $index:default-index)
};
