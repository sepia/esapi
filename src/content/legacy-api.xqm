xquery version "3.1";

module namespace lesapi="http://sepia.io/esapi/legacy-api";

import module namespace index="http://sepia.io/esapi/index";
import module namespace document="http://sepia.io/esapi/document";
import module namespace search="http://sepia.io/esapi/search";

declare namespace http="http://expath.org/ns/http-client";

(:~
 : API that mirrors the functions currently used by Architrave and KMG
 : with the $service parameter added.
 :
 : @deprecated New projects should not rely on this API because it is replaced
 : by the document, index and search API modules.
 :)
declare function lesapi:search($service as xs:string, $index, $query)
{
    search:query($service, $index, $query)
};

declare function lesapi:drop-index($service as xs:string, $index)
{
    index:remove($service, $index)
};

declare function lesapi:create-index($service as xs:string, $index, $data)
{
    index:create($service, $index, $data)
};

declare function lesapi:index-doc($service as xs:string, $index, $doc)
{
    document:add($service, $index, "", $doc)
};

declare function lesapi:index-doc($service as xs:string, $index, $id, $doc)
{
    document:add($service, $index, $id, $doc)
};

declare function lesapi:refresh($service as xs:string)
{
    index:refresh($service)
};
